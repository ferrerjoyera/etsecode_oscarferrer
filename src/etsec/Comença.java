/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package etsec;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Óscar
 */
public class Comença implements ActionListener {

    private Finestra f;
    public Comença(Finestra finestra ){
        f = finestra;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Etsec.tauler.activaModeJoc();
        f.comença();
        
        
    }
    
}
