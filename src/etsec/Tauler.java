/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package etsec;

import java.util.Random;

/**
 *
 * @author Óscar
 */
public class Tauler {

    public int getValue(int r, int c){
        return matriu[r][c];
    }
    
    
    private int[][] matriu;
    final private int nCartes, nParelles;
    private int  nFilesTotals, nColumnesTotals;
    private int nParellesFaltenPerTrobar;
    private boolean modeJoc = false;
    private int posicioDescobertaTemporal_1=-1,  posicioDescobertaTemporal_2=-1;
    private boolean pos1 = true;
    
    
    public Tauler(int nParelles){
        
        
        this.nCartes = nParelles*2;
        this.nParelles = nParelles;
        this.nParellesFaltenPerTrobar = nParelles;
        calculaNombreIdoniFilesColumnes();
        
        /*for(int i=2; i<10; i++)
            for(int j=3; j<10; j++)
                if(i*j==nParelles*2){
                    if(!almenysJaHeTrobatUnaConfiguracioValida || Math.abs(i-j) < millorI-millorJ){
                        millorI = i;
                        millorJ = j;
                    }
                    almenysJaHeTrobatUnaConfiguracioValida = true;
                }
        */

        
        initMatriu();
    }
    
    public void resetTorn(){
        
        posicioDescobertaTemporal_1 = -1;
        posicioDescobertaTemporal_2 = -1;
        
    }
    
    public int getTotalCartes(){
        return nCartes;
    }
    
    
    public void activaModeJoc(){
        modeJoc = true;
    }
    
    @SuppressWarnings("empty-statement")
    private void calculaNombreIdoniFilesColumnes(){
        //int millorI=2, millorJ=3;
        //boolean almenysJaHeTrobatUnaConfiguracioValida = false;
        float maxRatioRelacioAspecte = 3.0f;
        
        int numCols = (int) Math.sqrt(nCartes)+1;
         //aquest casting tallarà la part entera
        //ens sortirà la matriu més quadrada possible, allargada cap a l'ample
        int f,c;
        for(f=2; f<=10; f++)
            for(c=2; c<=10; c++)
                if(f*c==nCartes && (c/f) < maxRatioRelacioAspecte)
                {
                    nFilesTotals = f;
                    nColumnesTotals = c;
                    return;
                }
        int numFiles = 2;
        for(; numFiles*numCols < nCartes; numFiles++);
        nFilesTotals = numFiles;
        nColumnesTotals = numCols;
    }
    
    @Override
    public String toString(){
        String ret = "Matriu:\n";
        ret += "      ";
        for(int i=1; i<=nFilesTotals; i++){
            ret+=(i+": ");
        }
        ret +="\n";
        for(int i=0; i<nFilesTotals; i++){
            ret += "   "+(i+1)+": ";
            for(int j=0; j<nColumnesTotals; j++){
                if(modeJoc){
                    if(calculaPosicio(i,j)==posicioDescobertaTemporal_1
                    || calculaPosicio(i,j)==posicioDescobertaTemporal_2){
                        if(matriu[i][j] < 10)
                            ret += matriu[i][j]+"  ";
                        else ret+=matriu[i][j]+" ";
                    } else if(matriu[i][j]==0)
                        ret += "   ";
                    else ret += "x  ";
                }
                else if(matriu[i][j]==0)
                    ret+="   ";
                else if(matriu[i][j] < 10)
                    ret += matriu[i][j]+"  ";
                else ret+=matriu[i][j]+" ";
                
            }
            ret+="\n";
        }
        return ret;
    }

    private void initMatriu() {
        
        matriu = new int[nFilesTotals][nColumnesTotals];
        
        //prèviament Java inicialitza la matriu amb zeros ( 0 )
        Random r = new Random();
        for(int numero=1; numero<=nParelles; numero++){            
            
            //afegim el PRIMER element de la parella
            int posicioComposta = getNovaPosicioBuida();
            int fila = posicioComposta / nColumnesTotals;
            int columna = posicioComposta % nColumnesTotals;
            matriu[fila][columna] = numero;
            
            //afegim el SEGON element de la parella
            posicioComposta = getNovaPosicioBuida();
            fila = posicioComposta / nColumnesTotals;
            columna = posicioComposta % nColumnesTotals;
            matriu[fila][columna] = numero;
        }
    }
    
    private int getNovaPosicioBuida(){
        Random r = new Random();
        //retorna un int, amb la posicio composta buida que trobi
        int posicioComposta, fila, columna;
        do{
            posicioComposta = r.nextInt(nColumnesTotals*nFilesTotals);
            fila = posicioComposta / nColumnesTotals;
            columna = posicioComposta % nColumnesTotals;
        } while(matriu[fila][columna] != 0);  //al sortir d'aquest bucle ja hem trobat una posició buida
        //per augmentar l'eficiència hauríem de tenir una estructura de dades auxiliar on gestionéssim
        //les posicions buides i agafar la posició aleatòriament d'aquesta llista.
        //Però amb la quantitat de dades tan petita, no ho notarem i el programa funcionarà de forma relativament ràpida.
        return calculaPosicio(fila, columna);
    }
    
    private int calculaPosicio(int row, int col){
        return col + row*nColumnesTotals;
    }
    
    public int verifyDimensions(int row, int col) throws CoordinatesOutOfMatrixException {
        if(row < 1)
            throw new CoordinatesOutOfMatrixException("Please, do not introduce values lower than 1, try again.");
        else if (row > nFilesTotals)
        throw new CoordinatesOutOfMatrixException("Please, do not introduce values greater than "+ nFilesTotals+", try again.");
        if(col < 1)
            throw new CoordinatesOutOfMatrixException("Please, do not introduce values lower than 1, try again.");
        else if (col > nColumnesTotals)
        throw new CoordinatesOutOfMatrixException("Please, do not introduce values greater than "+ nColumnesTotals+", try again.");
        
        if(matriu[--row][--col] == 0)
            throw new CoordinatesOutOfMatrixException("Please, do not introduce blank positions.");
        
        return guardaPosicio(row, col);
    }
    
    public int guardaPosicio(int row, int col){
        
        int posicio = calculaPosicio(row,col);
        if(pos1)
            posicioDescobertaTemporal_1 = posicio;
        else //posicio Desc 2
            posicioDescobertaTemporal_2 = posicio;
        
        
        return posicio;
    }
    
    public void canviclic(){
        pos1 = !pos1;
    }
    
    public int getRows(){
        return nFilesTotals;
    }
    
    public int getCols(){
        return nColumnesTotals;
    }
    
    

/*    public int calculaPosicio(int row, int col) {
        return row*nColumnesTotals+col;
    }*/

    public boolean evaluaHeEncertat() {
        return matriu[posicioDescobertaTemporal_1/nColumnesTotals][posicioDescobertaTemporal_1%nColumnesTotals] ==
                matriu[posicioDescobertaTemporal_2/nColumnesTotals][posicioDescobertaTemporal_2%nColumnesTotals];
    }

    public void neteja() {
        if(posicioDescobertaTemporal_1 != -1){
            matriu[posicioDescobertaTemporal_1/nColumnesTotals][posicioDescobertaTemporal_1%nColumnesTotals] = 0;
            matriu[posicioDescobertaTemporal_2/nColumnesTotals][posicioDescobertaTemporal_2%nColumnesTotals] = 0;
            
            nParellesFaltenPerTrobar--;
            //if(nParellesFaltenPerTrobar > 0)
                //System.out.println("Has encertat! Torna a jugar.");
        }
    }

    public boolean shaAcabat() {
        return nParellesFaltenPerTrobar == 0;
    }
}
