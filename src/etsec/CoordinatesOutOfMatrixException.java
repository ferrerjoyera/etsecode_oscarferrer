/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package etsec;

/**
 *
 * @author Óscar
 */
class CoordinatesOutOfMatrixException extends Exception {

    public CoordinatesOutOfMatrixException(String message) {
        System.err.println(message);
    }

}
