/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package etsec;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Óscar
 */
public final class Finestra extends JFrame {
    
    private Tauler t;
    private JPanel botonera = new JPanel(), info = new JPanel();
    private JLabel puntuacion=new JLabel(), turno = new JLabel();
    private int rows, cols;
    private String[] imatgesNoms;
    private boolean primerClic = true;
    public static JButton src1, src2; //ultim clicat

   //private Container contenedor = getContentPane();
    
    public Finestra(Tauler t, boolean show){
        
        super("ETSECODE: Memory (Grup SOYPROGRAMADOR)");
        
        this.t = t;
        rows = t.getRows();
        cols = t.getCols();
        
        int rows = t.getRows(), cols = t.getCols();
        this.setSize(137*cols, 50 + 75*rows);
        this.setResizable(false);
        
        botonera = new JPanel();
        botonera.setLayout(new GridLayout(rows,cols));
        botonera.setSize(20*rows, 20*cols);
        
        imatgesNoms = Etsec.pokemon();
        JButton b;
        for(int i=0; i<rows; i++)
            for(int j=0; j<cols; j++){
                if(show){
                    ImageIcon ii = new ImageIcon(imatgesNoms[t.getValue(i,j)]+".png");
                    b = new JButton(ii);
                    b.setBackground(Color.white);
                } else{
                    
                    b = new JButton();
                    b.setBackground((t.getValue(i, j)==0) ? Color.white : Color.black);
                    b.addActionListener(new Clica(this));
                }
                
                //b.setIcon(new ImageIcon(imatgesNoms[t.getValue(i,j)]+".png"));
                botonera.add(b);    
            }
        
         //Para saber con exactitud el directorio de ejecución, puedes probar esto
         //System.out.println("Directorio ejecucion = " + System.getProperty("user.dir"));

        this.setLayout(new BorderLayout());
        
        panellInformatiu(show);

        
        this.add(botonera,BorderLayout.CENTER);
        this.add(info, BorderLayout.NORTH);
        
        actuPunts();
        
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    public void actuPunts(){
        puntuacion.setText("Punts "+Etsec.nom1()+": "+Etsec.cnt1+" - Punts "+Etsec.nom2()+": "+Etsec.cnt2);
        turno.setText("Torn "+Etsec.textTorn()+"       ");
        
    }
    
    public void comença(){
        this.setVisible(false);
        new Finestra(t,false);
    }
    
    public void panellInformatiu(boolean inicial){
        info = new JPanel();
        info.setBackground(Color.yellow);
        if(inicial){
            JButton b =new JButton("COMENÇA!!");
            b.addActionListener(new Comença(this));
            info.add(b);
            
        } else {
            botonera.setBackground(Color.cyan);
            puntuacion.setLayout(new FlowLayout());
            puntuacion.setBackground(Color.yellow);
            turno.setLayout(new FlowLayout());
            turno.setBackground(Color.YELLOW);   
            info.add(turno);
            info.add(puntuacion);
            
        }
        this.add(info, BorderLayout.NORTH);        
    }
    
    public void actualitzaFinestra(Object font){
        
        JButton src = (JButton) font;
        if(primerClic) src1 = src; else src2 = src;
        int fil,col;
        
        for(int c=0; c<t.getTotalCartes(); c++){
            fil=c/t.getCols();col = c%t.getCols();          
            if(botonera.getComponent(c).equals(font)){
                if(t.getValue(fil,col) == 0)return;
                t.guardaPosicio(fil,col);
                src.setIcon(new ImageIcon(imatgesNoms[t.getValue(fil,col)]+".png"));
                src.setBackground(Color.white);
                if(!primerClic){
                    if(!t.evaluaHeEncertat()){
                        canviDeTorn();
                        puntuacion.setText("Punts "+Etsec.nom1()+": "+Etsec.cnt1+" - Punts "+Etsec.nom2()+": "+Etsec.cnt2);
                        turno.setText("Torn "+Etsec.textTorn()+"       ");
                        new Thread(){
                            @Override
                            public void run(){
                                try {
                                    Thread.sleep(600);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Finestra.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                Finestra.src1.setBackground(Color.black);
                                Finestra.src1.setIcon(null);
                                Finestra.src2.setBackground(Color.black);
                                Finestra.src2.setIcon(null);
                            }
                        }.start();
                    } else {
                        Etsec.encertat();
                        new Thread(){
                            @Override
                            public void run(){
                                try {
                                    Thread.sleep(600);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(Finestra.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                Finestra.src1.setIcon(null);
                                Finestra.src2.setIcon(null);
                            }
                        }.start();
                        actuPunts();
                    }
                }
                break;
            }
        }
        if(t.shaAcabat()){
            Etsec.printaFinal();
        }
        primerClic = !primerClic;
        t.canviclic();
    }

    private void canviDeTorn() {
        if(Etsec.canviDeTorn())
            botonera.setBackground(Color.cyan); //jug1
        else botonera.setBackground(Color.ORANGE);
    }

}
