/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etsec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Óscar
 */
public class Etsec {

    private static String nom1, nom2;
    final private static String[] Pokemon = {
        "","pikachu","bulbasur","squirtle","charmander","tr","as","misty","charizard","blastoise","venasaur",
        "mew","mewto","pokeball","masterball","superball","aura","metapod","caterpie","buterfree","geodude",
        "ekans","koffing","cubone","chikorita","totodile","machop","pidgey","torchic","starmie","gengar",
        "cyndaquil","moltres","suicune","articuno","palkia","hooh","gyarados","magicarp","entei","giratina",
        "raiku","arceus","medallas","treecko","altaria","cubone","eevee","lugia","mudkip","zapdos"        
    };
    
    public static String[] pokemon(){
        return Pokemon;
    }
    public static String nom1(){return nom1;}
    public static String nom2(){return nom2;}
    
    static int cnt1 = 0, cnt2 = 0;
    static Tauler tauler;
    static Torn tornJugador = Torn.JUGADOR1;
    static String textTorn(){
        return (tornJugador.equals(Torn.JUGADOR1) ? nom1 : nom2);
    }
    static Scanner sc = new Scanner(System.in);
    static BufferedReader teclat = new BufferedReader(new InputStreamReader(System.in));
    static Finestra finestra;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException,
            ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {

        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        
        inicialitzaJoc();
        
        finestra = new Finestra(tauler, true);


/*        String returnKey = "valorSenseValor";
        try {
            returnKey = teclat.readLine();
        } catch (IOException e) {
            System.err.println("Error durant l'entrada, vigila què escrius!!");
            System.exit(0);
        }

        for (int i = 0; i < 100; i++) {
            System.out.println("\n");
        }
        tornJugador = Torn.JUGADOR1;
        tauler.activaModeJoc();

        if (returnKey.equals("")) {
            start(); //comencem!
        } else {
            System.err.println("Has premut massa botons! Torna a començar.");
            System.exit(0);
        }
*/
    }

    static public void printaFinal() {
        JOptionPane.showMessageDialog(finestra,"El joc s'ha acabat!!\nPuntuacio Jugador 1: " + cnt1+
                "\nPuntuacio Jugador 2: " + cnt2);
        if (cnt1 < cnt2) {
            JOptionPane.showMessageDialog(finestra,"El GUANYADOR ES EL JUGADOR 2!! Enhorabona :)");
        } else if (cnt2 < cnt1) {
            JOptionPane.showMessageDialog(finestra,"El GUANYADOR ES EL JUGADOR 1!! Enhorabona :)");
        } else {
            JOptionPane.showMessageDialog(finestra,"EL JOC HA ACABAT EN EMPAT.");
        }
        System.exit(0);
    }

    public static void inicialitzaJoc() throws IOException {
        
        int nombreDeParelles = 3;
        
        JOptionPane.showMessageDialog(null,"BENVINGUT AL ETSECODE!\n" +
            "Aquest projecte és l'exercici del grup: SoyProgramador, format per l'Òscar Ferrer.\n" +
            "He programat el joc 'Memory', tal com s'especifica en l'enunciat:\n");
        String input = JOptionPane.showInputDialog("Formació del tauler: Quantes parelles vols fer servir?");
        
        try{
            nombreDeParelles = Integer.parseInt(input);
            if(nombreDeParelles < 3 || nombreDeParelles > 50)
                throw new NumberFormatException();
        } catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Aquest número és incorrecte!! (entre 3 i 50)");
            System.exit(0);}
        
        System.out.println(nombreDeParelles);
       // System.out.println("BENVINGUT AL ETSECODE!");
       // System.out.println("Aquest projecte és l'exercici del grup: SoyProgramador, format per l'Òscar Ferrer.");
       // System.out.println("He programat el joc 'Memory', tal com s'especifica en l'enunciat:");

      // System.out.println("Formació del tauler: Quantes parelles vols fer servir?");

        nom1 = JOptionPane.showInputDialog("Jugador 1, quin és el teu nom?");
        nom2 = JOptionPane.showInputDialog("Jugador 2, quin és el teu nom?");
        
  /*      try {
            nombreDeParelles = sc.nextInt();
            if (nombreDeParelles < 3 || nombreDeParelles > 50) {
                throw new NombreIncorrecteDeParellesException();
            }
        } catch (NombreIncorrecteDeParellesException e) {
            System.err.println("Has introduït un nombre incorrecte de parelles!! (Entre 3 i 50, inclosos).");
            System.exit(0);
        } catch (Exception e) {
            System.err.println("Has introduït un nombre incorrecte!");
            System.exit(0);
        }*/

        tauler = new Tauler(nombreDeParelles);
        

       // System.out.println(tauler);

//        System.out.println("Sisplau, prem RETURN per començar la partida:");
    }

    /*private static void start() {
        while (!tauler.fiDelJoc()) {

            tauler.resetTorn();

            //jugador tria 2 peces
            choosePiece();
            System.out.println(tauler);
            choosePiece();
            System.out.println(tauler);
            boolean pucSeguir = tauler.evaluaHeEncertat();
            if (!pucSeguir) {
                canviDeTorn();
            } else { 
                encertat();
            }
        }

        printaFinal();
    }
    */
    
    public static void encertat(){
        //has encertat!!
                tauler.neteja();
                if (tornJugador.equals(Torn.JUGADOR1)) {
                    cnt1++;
                } else //jugador 2
                {
                    cnt2++;
                }
    }

    private static int choosePiece() {
        int row = -1, col = -1;
        boolean wellIntroduced = false;
        int piece = -1; //it does not matter
        while (!wellIntroduced) {
            try {
               // System.out.println(tornJugador + ", sisplau, introdueix 2 numeros separats (fila i columna, respectivament, des de 1 fins a N):");
              //  System.out.println("Recorda el tamany del tauler: Files: 1 fins a " + tauler.getRows() + ", Columnes: 1 fins a " + tauler.getCols());
                row = sc.nextInt();
                col = sc.nextInt();
                piece = tauler.verifyDimensions(row, col);
                wellIntroduced = true;
            } catch (CoordinatesOutOfMatrixException e) {
            } //el print el faig a la classe excepcio
        }
        return piece;
    }

    public static boolean canviDeTorn() {
        if (tornJugador.equals(Torn.JUGADOR1)) {
            tornJugador = Torn.JUGADOR2;
            return false;
        } else {
            tornJugador = Torn.JUGADOR1;
            return true;
        }
   
    }
}
